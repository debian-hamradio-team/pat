pat (0.16.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.16.0
  * Add Build-Dep on golang-golang-x-sync-dev

 -- tony mancill <tmancill@debian.org>  Mon, 29 Jul 2024 20:39:41 -0700

pat (0.15.1-2) unstable; urgency=medium

  * Team upload.
  * Upload build that enables PACTOR to unstable.
  * Bump Standards-Version to 4.7.0

 -- tony mancill <tmancill@debian.org>  Sun, 23 Jun 2024 20:58:59 -0700

pat (0.15.1-2~exp1) experimental; urgency=medium

  * Team upload to experimental.
  * Enable PACTOR (Closes: #1057276)

 -- tony mancill <tmancill@debian.org>  Fri, 29 Dec 2023 13:45:38 -0800

pat (0.15.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.15.1
  * Add build-dep on golang-github-kelseyhightower-envconfig-dev

 -- tony mancill <tmancill@debian.org>  Fri, 08 Dec 2023 21:13:22 -0800

pat (0.15.0-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches for new upstream version
    including vendor pat-vara to v1.1.2
  * Freshen years in debian/copyright
  * Bump Standards-Version to 4.6.2

 -- Federico Grau <donfede@casagrau.org>  Sat, 08 Jul 2023 11:17:29 -0400

pat (0.13.1-1) unstable; urgency=medium

  * New upstream release

 -- Federico Grau <donfede@casagrau.org>  Sat, 22 Oct 2022 18:16:34 -0400

pat (0.13.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- tony mancill <tmancill@debian.org>  Sat, 15 Oct 2022 15:00:10 -0700

pat (0.13.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 0.13.0
  * Refresh patches for new upstream version
  * Freshen years in debian/copyright
  * Bump Standards-Version to 4.6.1

 -- tony mancill <tmancill@debian.org>  Thu, 01 Sep 2022 21:04:51 -0700

pat (0.12.1-3) unstable; urgency=medium

  * Team upload.
  * Link against libax25 for AX25 support

 -- tony mancill <tmancill@debian.org>  Sat, 11 Jun 2022 19:30:38 -0700

pat (0.12.1-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + pat: Drop versioned constraint on gpsd in Suggests.

  [ Federico Grau ]
  * Merge Janitor's work, and release update

 -- Christoph Berg <myon@debian.org>  Mon, 14 Mar 2022 10:29:04 +0100

pat (0.12.1-1) unstable; urgency=medium

  * new upstream release

 -- Taowa <taowa@debian.org>  Fri, 11 Feb 2022 10:35:28 -0500

pat (0.12.0-1) unstable; urgency=medium

  * New upstream release (skipping 0.11.0)

 -- Federico Grau <donfede@casagrau.org>  Sun, 14 Nov 2021 15:21:50 -0500

pat (0.10.0-2) unstable; urgency=medium

  * Rename binary and man page; d/rules README.Debian edits (Closes: #994822)
    and 04-update_man_pages_for_renamed_binary_994822.patch
  * Update d/control Standards to 4.6.0

 -- Federico Grau <donfede@casagrau.org>  Wed, 13 Oct 2021 22:14:42 -0400

pat (0.10.0-1) unstable; urgency=medium

  [ Taowa ]
  * Packaging v0.9.0 work, including d/copyright research, d/patches, and more

  [ Federico Grau ]
  * Initial release (Closes: #877030)

 -- Federico Grau <donfede@casagrau.org>  Mon, 03 May 2021 21:34:44 +0200
